from flask import Blueprint
from flask_restful import Api

from jroom.Resource import ConferenceRoomResource

api_bp = Blueprint("api", __name__)
api = Api(api_bp)

api.add_resource(ConferenceRoomResource, "/room")
