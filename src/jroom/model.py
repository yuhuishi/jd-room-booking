import datetime

from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
ma = Marshmallow()


class ConferenceRoom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    floor = db.Column(db.Integer)
    capacity = db.Column(db.Integer)
    has_tv = db.Column(db.Boolean)
    room_no = db.Column(db.String(100))

    def __repr__(self):
        return "<Conference room>: {name} @ floor {floor}".format(name=self.name,
                                                                  floor=self.floor)


class Bookings(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey("conference_room.id"), nullable=False)
    room = db.relationship("ConferenceRoom", backref=db.backref("bookings", lazy=True),
                           uselist=False)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    user = db.relationship("User", backref=db.backref("bookings", lazy=True),
                           uselist=False)

    book_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())
    start_time = db.Column(db.DateTime, nullable=False)
    end_time = db.Column(db.DateTime, nullable=False)

    is_canceled = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return "<Booking> room: {room_id} - booking time: {book_time} - start: {start_time} - end: {end_time}" \
            .format(room_id=self.room_id,
                    book_time=self.book_time,
                    start_time=self.start_time,
                    end_time=self.end_time)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    dept_id = db.Column(db.Integer, db.ForeignKey("department.id"))
    department = db.relationship("Department", backref=db.backref("users", lazy=True),
                                 uselist=False, foreign_keys=[dept_id])

    def __repr__(self):
        return "<User> name: {name} - dept: {dept_id}".format(
            name=self.name,
            dept_id=self.dept_id
        )


class Department(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(100), nullable=False, unique=True)
    abbrev_name = db.Column(db.String(50), nullable=False, unique=True)
    contact_person_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    contact_person = db.relationship("User", backref=db.backref("leading_dept", lazy=True),
                                     uselist=False, foreign_keys=[contact_person_id])

    def __repr__(self):
        return "<Department> abbrev: {abbrev} - name: {full_name} - contact: {contact_person}".format(
            abbrev=self.abbrev_name,
            full_name=self.full_name,
            contact_person=self.contact_person_id
        )


# ======= Marshmallow models =============
class ConferenceRoomSchema(ma.ModelSchema):
    class Meta:
        model = ConferenceRoom


class BookingSchema(ma.ModelSchema):
    class Meta:
        model = Bookings
    room = ma.HyperlinkRelated("room_detail")
    user = ma.HyperlinkRelated("user_detail")


class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
    department = ma.HyperlinkRelated("dept_detail")


class DepartmentSchema(ma.ModelSchema):
    class Meta:
        model = Department

