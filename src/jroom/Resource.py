from flask_restful import Resource

from .model import ConferenceRoom, ConferenceRoomSchema

room_schema = ConferenceRoomSchema()
rooms_schema = ConferenceRoomSchema(many=True)


class ConferenceRoomResource(Resource):
    def get(self):
        rooms = ConferenceRoom.query.all()
        rooms = rooms_schema.dump(rooms).data
        return {
                   "stats": "success",
                   "data": rooms
               }, 200
